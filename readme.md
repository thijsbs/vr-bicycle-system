# VR bicycle tracking system

This is a little hobby project where I had the idea to make a system where an exercise bike can be turned into a virtual bicycle experience.

By tracking the position of the bicycle pedals, the position of the legs can be tracked such that the virtual body corresponds with the physical one. Also, the experience of reaching out to your virtual bicycle handlebar and feeling a corresponding physical handlebar would add to the immersion.

The idea was to create this system and let the user cycle through beautiful landscapes along a set track, purely as a visual experience accompanying an exercise, or a relaxing bike ride. The only thing that the user would be able to change is the speed of the virtual bike, which corresponds to the tracked physical bike.

The project is in a half-finished state at the moment as I didn't even own a VR headset or an exercise bike at the time. 
It ended up being a fun exercise to get an arduino to talk with the Unity engine.

I might pick this project up again for fun when I finally get around to getting an exercise bike.
