/* Bike controller version 1 - rotary encoder
This bike controller sketch is meant to read rotary encoder clk and dt signals
The sketch performs the following functions
- Track the rotary position
- When enabled, send the rotary position through the serial port as it comes in
- An actuator can be controlled based on a resistance level that is passed in via the serial port
- Information requests can be made via the serial communication
- Each device should have a unique arduinoID!

All serial communication is enclosed in '<' '>' brackets to make sure all information is received properly

List of serial commands:
- <i>:   Request arduinoID. The arduino will send back the ID (example '<0001>')
- <s>:   Request the amount of encoder steps. The arduino will send back the amount of encoder steps (ex. '<40>')
- <p>:   Request the current rotary position. The arduino will send back the current position (ex. '<38>')
- <u>:   Start the direct position communication mode. The arduino will send the current position as soon as it changes (ex. <22>)
- <b>:   Block the direct position communication mode. The arduino will not send position changes directly.
- <r00>: Set a resistance level. The code 'r' should be followed by a 2 digit number.
 */


const String arduinoID = "0001";  // Unique id of the controller
int clk = 2;                      // Connected to rotary encoder clk pin
int dt = 3;                       // Connected to rotary encoder dt pin
int pos = 0;                      // Current rotary position
int totalSteps = 40;              // Total amount of steps on the rotary encoder
int state;                        // The rotary encoder clk state
int lastState;                    // The previous rotary encoder clk state
int resistance = 0;               // Resistance level with which to actuate the brake (from 0 to 99)
bool block = true;                // Wether direct position communication should be blocked

// Variables to handle incoming characters
const byte numChars = 4;          // The serial input buffer size (not including start/end markers)
char serialInput[numChars];       // an array to store the received data
boolean newData = false;          // True if the serial input has been updated
char startMarker = '<';
char endMarker = '>';

void setup() {
  pinMode(clk, INPUT);
  pinMode(dt, INPUT);
  pinMode(13, OUTPUT);
  lastState = digitalRead(clk);
  Serial.begin(9600);
}

void loop() 
/* The main loop of the program
 * The main loop does two things:
 * 1) Update the rotary position
 * 2) Listen for serial communication
 */
{
 
  // --- Update rotary position ---
  
  state = digitalRead(clk);
  if (state != lastState)
  // Check if there was rotational movement
  {
    if (state != digitalRead(dt))
    {
      // Increment pos if encoder signal and dt signal are not the same
      pos ++;
    }
    else
    {
      // Decrement pos if encoder signal and dt signal are the same
      pos --;
    }
    // The position should be between 0 and the total encoder steps
    pos = LoopedPos(pos); 

    if (!block)
    {
        // Send the new rotary position
        Serial.println(Decorate(pos));
    }
  }
  lastState = state;


// --- Listen for serial requests ---

  receiveData(); 
  
  if (newData)
  // If a new command has been received, check if it is a recognized command and execute
  {
    switch (serialInput[0]) 
    {
      case 'i':    // ArduinoID request
        SendID();
        break;
      case 'r':    // Set resistance level
        SetResistance(serialInput);
        break;
      case 's':    // Total encoder steps request
        SendSteps();
        break;
      case 'p':    // Rotary position request
        SendPosition();
        break;
      case 'u':    // Unblock direct position communication
        SetReady();
        break;
      case 'b':    // Block direct position communication
        Block();
        break;
    }
    newData = false;
  }
}


void receiveData()
/* This function listens to the serial port for any recognized commands
 * If new data has come in the newData flag will be set to true
*/
{
  static boolean recvInProgress = false;
  static byte ndx = 0; // Current character index in the buffer
  char rc; // Current character

  while (Serial.available() > 0 && newData == false) 
  {
    rc = Serial.read();

    if (recvInProgress == true) 
    {
      if (rc != endMarker) 
      {
        // Add each character in the bufer to the received chars untill the endmarker is detected
        serialInput[ndx] = rc;
        ndx++;
        if (ndx >= numChars) 
        {
          // Overwrite the last received char if the received char buffer is full
          // The last character is reserved for line ending anyway
          ndx = numChars - 1;
        }
      }
      else
      {
        // Finish the string and reset for the next loop
        serialInput[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = true;
      }
    }
    else
    {
      if (rc == startMarker)
      {
        // If the start marker is detected, start receiving subsequent characters
        recvInProgress = true;
      }
    }
  }
}


void SendID()
{
  Serial.println(Decorate(arduinoID));
}


void SendPosition()
{
  Serial.println(Decorate(pos));
}


void SendSteps()
{
  Serial.println(Decorate(totalSteps));
}


void SetReady()
// Unblock the direct rotary position communication
{
  block = false;
  Serial.println(Decorate("go"));
  digitalWrite(13, HIGH);
}


void Block()
// Block the direct rotary position communication
{
  block = true;
  Serial.println(Decorate("stop"));
  digitalWrite(13, LOW);
}


void SetResistance(char chars[])
/* Set the desired resistance level
 * 
 * Input: character array with the first char being a letter, followed by two numbers (ex. "r50")
 */
{
  char buf[2];
  buf[0] = chars[1];
  buf[1] = chars[2];
  resistance = atoi(buf);
  ApplyResistance();
}


void ApplyResistance()
//Actuates the brake to match the resistance level
{
  // Not implemented yet
}


String Decorate(int val)
/* Decorate an integer value with the start/end characters
 * Input: An integer value
 * Output: A decorated string
 */
{
  return String(startMarker)+String(val)+String(endMarker);
}


String Decorate(String val)
/* Decorate a string with the start/end characters
 * Input: A string value
 * Output: A decorated string
 */
{
  return String(startMarker)+val+String(endMarker);
}


int LoopedPos(int pos)
/* Map a position value between 0 and totalSteps
 * Input: An integer value encoding an unmapped rotary position
 * Output: An integer value begween 0 and totalSteps
 */
{
  while (pos >= totalSteps)
  {
    // when pos exceeds the totalSteps, loop back one whole circle
    pos -= totalSteps;  
  }

  while (pos < 0)
  {
    // when pos is negative, loop forward one whole circle
    pos += totalSteps;
  }
  return pos;
}
