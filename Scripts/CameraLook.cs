﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLook : MonoBehaviour

{
    public float mouseSensitivity = 100.0f;
    public float clampAngle = 80.0f;
 
    private float rotY = 0.0f; // rotation around the up/y axis
    private float rotX = 0.0f; // rotation around the right/x axis
    private bool freeMouseMode = false;
 
    void Start ()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

 
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
        	if (freeMouseMode)
        	{
	        	freeMouseMode = false;
	            Cursor.visible = false;
	            Cursor.lockState = CursorLockMode.Locked;
        	}
        	else
        	{
	        	freeMouseMode = true;
	            Cursor.visible = true;
	            Cursor.lockState = CursorLockMode.None;
        	}
        }
 
 		if (!freeMouseMode)
 		{
	        float mouseX = Input.GetAxis("Mouse X");
	        float mouseY = -Input.GetAxis("Mouse Y");
	        rotY += mouseX * mouseSensitivity * Time.deltaTime;
	        rotX += mouseY * mouseSensitivity * Time.deltaTime;
	 
	        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);
	        
	        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
	        transform.rotation = localRotation;
        }
    }
}

