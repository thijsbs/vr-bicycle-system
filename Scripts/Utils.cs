// Implementations of genereric utility functions
using System.Collections;
using System.Collections.Generic;


public class Utils
// A class containing utility functions
{


    public static float[] Linspace(float x1, float x2, int n, bool endPoint=true)
    /* Return an array of linearly spaced values
        Input:
            - float x1: The starting value of the interval (included in the array)
            - float x2: The last value of the interval (only included in the array if specified)
            - int n: The amount of values to span the interval
            - bool endPoint: Wether or not to add the final value x2 in the array

        Output:
            - float[]: An array of equally spaced points between x1 and x2 with n values.
    */
    {
        float[] y = new float[n];
        float stepSize;

        if (endPoint)
        {
            stepSize = (x2-x1)/(n-1);
        }
        else
        {
            stepSize = (x2-x1)/n;
        }

        for (int i = 0; i < n; i++)
        {
            y[i] = x1 + stepSize * i;
        }
        return y;
    }
}
