﻿/* 
This script takes a spline path object and discretizes it 
for runtime use.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathDiscretizor : MonoBehaviour
{

    public float curveResolution = 100f;    // Curve sampling resolution
    public float pointDistance = 0.2f;      // Distance between discretized points
    public bool debugSpheres = false;       // Debug mode
    public Material mat;                    // Material used for debug objects
    public float debugSphereSize = 0.2f;    // Size of the debug spheres
    private Path path;                      // Reference to the continuous path
    private Vector3[] points;               // Array of discretized points


    void Start()
    {
        path = transform.gameObject.GetComponent<PathCreator>().path;
        points = path.CalculateEvenlySpacedPoints(pointDistance, curveResolution);

        if (debugSpheres && mat != null)
        {
            // If in debug mode show all the descretized points
            print("Number of descretized points: "+points.Length.ToString());
            Vector3 scl = new Vector3(debugSphereSize, debugSphereSize, debugSphereSize);
            foreach(Vector3 p in points)
            {
                GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                g.transform.position = p;
                g.transform.localScale = scl;
                g.GetComponent<Renderer>().material = mat;
            }   
        }
    }


    public Vector3 GetPositionFromDistance(float distance)
    /*  Get the world position along the path (interpolated between discretized points) 
        based on the distance from the starting point
            Input: float, the distance along the discretized path
            Output: Vector3, the world position corresponding to the distance on the path
    */
    {
        int i = Mathf.FloorToInt(distance/pointDistance);
        float t = (distance - i*pointDistance) / pointDistance;
        return Vector3.Lerp(points[LoopIndex(i)], points[LoopIndex(i+1)], t);
    }


    public int LoopIndex(int i)
    /*  Loops the point index around for closed curves when the index exceeds the range 
        If the path is open the index is clipped to the min/max range.
            Input: int, the un-looped index
            Output: int, the looped index
    */
    {
        if (i < 0)
        {
            if (path.Closed)
            {
                return i + points.Length * Mathf.CeilToInt(i/points.Length);
            }
            else
            {
                return 0;
            }
        }
        if (i > points.Length-1)
        {
            if (path.Closed)
            {
                return i - points.Length * Mathf.FloorToInt(i/points.Length);
            }
            else
            {
                return points.Length-1;
            }
        }
        return i;
    }
}
