using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathCreator))]
public class PathEditor : Editor
{
    PathCreator creator;
    Path path;
    bool showControlPoints = false;
    bool showAnchorPoints = true;
    bool pathTraceTest = false;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();
        if (GUILayout.Button("Reset"))
        {
            Undo.RecordObject(creator, "Reset path");
            creator.CreatePath();
            path = creator.path;
        }

        if (GUILayout.Button("Toggle closed"))
        {
            Undo.RecordObject(creator, "Toggle close path");
            path.ToggleClosed();
        }

        bool autoSetControlPoints = GUILayout.Toggle(path.AutoSetControlPoints, "Auto Set Controls");
        if (autoSetControlPoints != path.AutoSetControlPoints)
        {
            Undo.RecordObject(creator, "Toggle auto set controls");
            path.AutoSetControlPoints = autoSetControlPoints;
        }

        showAnchorPoints = GUILayout.Toggle(showAnchorPoints, "Show anchor points");
        showControlPoints = GUILayout.Toggle(showControlPoints, "Show control points");
        pathTraceTest = GUILayout.Toggle(pathTraceTest, "Show path tracer");


        if (EditorGUI.EndChangeCheck())
        {
            SceneView.RepaintAll();
        }
    }

    void OnSceneGUI()
    {
        Input();
        Draw();
    }


    void Input()
    {
        Event guiEvent = Event.current;
        Ray mouseRay = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);

        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 && guiEvent.shift)
        {
            RaycastHit hit;
            Vector3 newPos;
            if(Physics.Raycast(mouseRay, out hit))
            {
               newPos = hit.point;
            }
            else
            {
                float camdist = Vector3.Distance(path[path.NumPoints-1], SceneView.currentDrawingSceneView.camera.transform.position);
                newPos = mouseRay.GetPoint(camdist);
            }
            Undo.RecordObject(creator, "Add Segment");
            path.AddSegment(newPos);
        }

        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 1)
        {
            int closestAnchorIndex = -1;
            float minDstToAnchor = creator.markerSize;
            
            for (int i = 0; i < path.NumPoints; i+=3)
            {
                float dst = Vector2.Distance(mouseRay.origin, path[i]);
                if (dst < creator.markerSize)
                {
                    minDstToAnchor = dst;
                    closestAnchorIndex = i;
                }
            }
            
            if (closestAnchorIndex != -1)
            {
                Undo.RecordObject(creator, "Delete segment");
                path.DeleteSegment(closestAnchorIndex);
            }
        }

    }

    void Draw()
    {
        for (int i = 0; i < path.NumSegments; i++)
        {
            Vector3[] points = path.GetPointsInSegment(i);
            Handles.DrawBezier(points[0], points[3], points[1], points[2], Color.green, null, 5);
            if (showControlPoints)
            {
                Handles.DrawLine(points[0], points[1]);
                Handles.DrawLine(points[2], points[3]);
            }
        }


        for (int i = 0; i< path.NumPoints; i++)
        {
            if (i % 3 == 0)
            {
                if (!showAnchorPoints)
                {
                    continue;
                }
                Handles.color = Color.red;
            }
            else
            {
                if (!showControlPoints)
                {
                    continue;
                }
                Handles.color = Color.white;
            }
            Vector3 newPos = Handles.FreeMoveHandle(path[i], Quaternion.identity, creator.markerSize, Vector3.zero, Handles.SphereHandleCap);
            if (path[i] != newPos)
            {
                Undo.RecordObject(creator, "Move point");
                path.MovePoint(i, newPos);
            }

        }
        if (pathTraceTest)
        {
            Handles.color = Color.blue;        
            Handles.FreeMoveHandle(path.GetPositionAtDistance(creator.distPositionTest), Quaternion.identity, 1f, Vector3.zero, Handles.SphereHandleCap);
        }


    }


    void OnEnable()
    {
        creator = (PathCreator)target;
        if (creator.path == null)
        {
            creator.CreatePath();
        }
        path = creator.path;

    }
}
