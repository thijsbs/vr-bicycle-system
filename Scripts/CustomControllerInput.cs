using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using System.IO.Ports;


public class CustomControllerInput : MonoBehaviour
{
    public ArduinoController ac;

    public int readDelay = 50;
    public int openDelay = 2000;

    private BikeController bikeControl;
    private int newPosition;
    private int prevPosition;
    private int maxSteps;
    private int controllerPos;


    void Start()
    {
        bikeControl = transform.GetComponent<BikeController>();
        ac = new ArduinoController("/dev/ttyACM0", 9600, readDelay, openDelay);
        print(ac.id);
        print(ac.rotationSteps);
        maxSteps = ac.rotationSteps; // Get the controller steps from the controller itself
    }


    void FixedUpdate()
    {
        newPosition = ac.ListenForPositionUpdate();
        if (newPosition > -1) // if the position is valid
        {
            int diff = LoopedDiff(prevPosition, newPosition, maxSteps);

            if (Mathf.Abs(diff) < 5)
            {
                for (int i = 0; i < Mathf.Abs(diff); i++)
                {
                    if (diff > 0)
                    {
                        bikeControl.IncrementPedal();
                    }
                    else
                    {
                        bikeControl.DecrementPedal();
                    }
                }
            }
                prevPosition = newPosition;
        }
    }


    private int LoopedDiff(int p1, int p2, int maxSteps, int loopingTolerance=5)
    {
        int diff = p2 - p1;

        if (diff >= maxSteps - loopingTolerance)
        {
            diff = -maxSteps + p2 - p1;
        }

        if (diff <= -maxSteps + loopingTolerance)
        {
            diff = maxSteps - p1 + p2;
        }
        // print($"p1: {p1}, p2: {p2}, diff: {diff}");
        return diff;
    }


}
