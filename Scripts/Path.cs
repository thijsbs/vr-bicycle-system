﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Path
// This class handles all the math to define a bezier spline
// The Bezier spline path is built up from anchor points and tangent control points
// The entire path can be split up in path segments, each its own bezier spline
// Each path segment is defined by a set of 4 points: 2 anchor points and 2 control points in between
{
    [SerializeField, HideInInspector]
    List<Vector3> points;               // The control points that define the bezier spline
    [SerializeField, HideInInspector]
    List<float> segmentLengths;         // The length of each path segment
    [SerializeField, HideInInspector]
    bool isClosed;                      // Determines wether or not the path should be a closed loop or open
    [SerializeField, HideInInspector]
    bool autoSetControlPoints;          // When set to true calculates and sets all the tangent points

    int samplePointsPerSegment = 10;    // The accuracy of the distance estimation


    public Path(Vector3 center)
    /* Initializes the path object with 4 control points around a given center point
        Input: Vector3, a world position around which to place the first 4 control points
    */
    {
        points = new List<Vector3>
        {
            center+Vector3.left,
            center+Vector3.left+Vector3.forward*0.5f,
            center+Vector3.right+Vector3.back*0.5f,
            center+Vector3.right
        };
        segmentLengths = new List<float> {GetSegmentLength(0, samplePointsPerSegment)};
    }


    public Vector3 this[int i]
    // Get a path control point from an index 
    {
        get
        {
            return points[i];
        }
    }


    public int NumPoints
    // Get the total number of points
    {
        get
        {
           return points.Count; 
        }
    }

    public int NumSegments
    // Get the total number of path segments
    {
        get
        {
            return points.Count/3;
        }
    }

    public bool Closed
    // Get the state of the isClosed flag
    {
        get
        {
            return isClosed;
        }
    }

    public float PathLength
    // Get an estimated total length of the path
    {
        get
        {
            return segmentLengths[segmentLengths.Count-1];
        }
    }

    public bool AutoSetControlPoints
    // Get/set the AutoSetControlPoints flag and change the points if set to true
    {
        get
        {
            return autoSetControlPoints;
        }
        set
        {
            if (autoSetControlPoints != value)
            {
                autoSetControlPoints = value;
                if (autoSetControlPoints)
                {
                    // Calculate all the tangent control points and change them
                    AutoSetAllControlPoints();
                }
            }
        }
    }


    public void AddSegment(Vector3 anchorPos)
    /* Add a new path segment by adding 4 new control points
        Input: Vector3, the new anchor position to add
    */
    {
        points.Add(points[points.Count-1]*2 - points[points.Count-2]);
        points.Add((points[points.Count-1] + anchorPos) * 0.5f);
        points.Add(anchorPos);

        if (autoSetControlPoints)
        {
            AutoSetAllAffectedControlPoints(points.Count-1);
        }
        segmentLengths.Add(segmentLengths[segmentLengths.Count-1] + GetSegmentLength(NumSegments-1, samplePointsPerSegment));
    }


    public void DeleteSegment(int anchorIndex)
    {
        if (NuimSegments > 2 || !isClosed && NumSegments > 1)
        {       
            if (anchorIndex == 0)
            {
                if (isClosed)
                {
                    points[points.Count - 1] = points[2];
                }
                points.RemoveRange(0, 3);
            }
            else if (anchorIndex == points.Count - 1 && !isClosed)
            {
                points.RemoveRange(anchorIndex -2, 3);
            }
            else
            {
                points.RemoveRange(anchorIndex - 1, 3);
            }
        }
    }


    public Vector3[] GetPointsInSegment(int i)
    /* Returns the corresponding control points of a certain path segment
        Input: int, the index of the path segment
        Output: Vector3[], an array of control points corresponding to the path segment
    */
    {
        return new Vector3[]{points[i*3], points[i*3+1], points[i*3+2], points[LoopIndex(i*3+3)]};
    }

    public Vector3[] SampledSegment(int i, int samplePoints)
    /* Define a set of points along a path segment
        Input:  - int i: The index of the path segment
                - int samplePoints: The amount of points to define along the path segment
        Output: - Vector3[]: array of points along the path segment with length of samplePoints
    */
    {
        Vector3[] sPoints = GetPointsInSegment(i);
        Vector3[] samples = new Vector3[samplePoints];
        float t = 0f;
        for (int j=0; j < samplePoints-1; j++)
        {
            // Calculate each point on the bezier curve at fixed intervals of t
            // Note that eual intervals of t do not correspond to equal intervals in distance
            samples[j] = Bezier.EvaluateCubic(sPoints[0], sPoints[1], sPoints[2], sPoints[3], t);
            t += 1f / (samplePoints-1);
        }
        samples[samplePoints-1] = Bezier.EvaluateCubic(sPoints[0], sPoints[1], sPoints[2], sPoints[3], 1f);
        return samples;
    }


    public float GetSegmentLength(int i, int samplePoints)
    /* Return an estimated length of a path segment. The segment is divided up into linear sub-segments
       the length of which is added together. The amount of sample points / sub-segments determines
       the accuracy of the estimation.
        Input:  - int i: The index of the path segment
                - int samplePoints: The amount of points with which to sample the path segment
        Output: - float: the estimated length of the path segment
    */
    {
        Vector3[] sPoints = GetPointsInSegment(i);
        float dist = 0f;
        Vector3 lastPoint = sPoints[0];
        Vector3 newPoint;

        foreach(float t in Utils.Linspace(0f, 1f, samplePoints))
        {
            // Divide the path segment up in linear sub-segments and sum the distance
            newPoint = Bezier.EvaluateCubic(sPoints[0], sPoints[1], sPoints[2], sPoints[3], t);
            dist += Vector3.Distance(lastPoint, newPoint);
            lastPoint = newPoint;
        }
        return dist;
    }


    public void UpdatePathLegths(int samplePoints = 10)
    /* Update the cached lengths of each path segment for later lookup
       This gets updated only when the path changes so the calculation does not have to
       be done everytime this information is required (possibly at runtime?)
        Input:
            - int samplePoints: The amount of points with which to sample the path segments
    */
    {
        float dist = 0f;
        for (int i = 0; i < NumSegments; i++)
        {
            dist += GetSegmentLength(i, samplePoints);
            segmentLengths[i] = dist;
        }
    }
    

    public float GetTotalLength(int samplePointsPerSegment = 10)
    /* Calculate and return the estimated total length of the path
       This function can be used when the path length needs to be calculated, instead of
       with the normal lookup.
        Input: 
            - int samplePoitsPerSegment: The amount of points with which to sample the path segments
        Output:
            - float: the estimated total length of the path
    */
    {
        float dist = 0f;
        for (int i = 0; i < NumSegments; i++)
        {
            dist += GetSegmentLength(i, samplePointsPerSegment);
        }
        return dist;
    }


    public void MovePoint(int i, Vector3 pos)
    /* Update the position of control point i
        Input:
            - int i: the control point index
            - Vector3 pos: The new position of the control point
    */
    {
        Vector3 deltaMove = pos - points[i];

        if (i%3==0 || !autoSetControlPoints)
        // Only allow editing tangent control points if the autoSetControlPoints flag is false
        {     
            points[i] = pos;

            if (autoSetControlPoints)
            // Automatically update the tangent control points if enabled
            {
                AutoSetAllAffectedControlPoints(i);
            }
            else
            {
                if (i % 3 == 0)
                {
                    // If the path is closed also update the corresponding trangent control points
                    // at the other end of the path
                    if (i+i < points.Count || isClosed)
                    {
                        points[LoopIndex(i+1)] += deltaMove;
                    }
                    if (i-1 >= 0 || isClosed)
                    {
                        points[LoopIndex(i-1)] += deltaMove;
                    }
                }
                else
                {
                    // If an anchor point is moved, also move its tangent control points with it
                    bool nextPointIsAnchor = (i+1) % 3 == 0;
                    int correspondingControlIndex = (nextPointIsAnchor) ? i+2 : i-2;
                    int anchorIndex = LoopIndex((nextPointIsAnchor) ? i+1 : i-1);

                    if (correspondingControlIndex >= 0 && correspondingControlIndex < points.Count || isClosed)
                    {
                        // If this control point also loops around a closed path, update this as well
                        float dst = (points[anchorIndex] - points[LoopIndex(correspondingControlIndex)]).magnitude;
                        Vector3 dir = (points[anchorIndex] - pos).normalized;
                        points[LoopIndex(correspondingControlIndex)] = points[anchorIndex] + dir*dst;
                    }
                }
            }
        }
        UpdatePathLegths(samplePointsPerSegment);
    }


    public int GetSegmentIndexFromDistance(float dist)
    /* Determine which segment corresponds to a given distance
        Input:
            - float dist: the given distance
        Output:
            - int: The index of the corresponding path segment
    */
    {
        // First determine if the distance is more than one or multiple loops
        if (dist >= PathLength)
        {
            if (isClosed)
            {
                float loops = Mathf.Floor(dist/PathLength);
                dist -= loops*PathLength;
            }
            else
            {
                return NumSegments-1;
            }
        }

        // Keep incrementing the segment index until the accumulated segment lengths 
        // are gretaer than the target distance
        int segmentIndex = 0;
        while(segmentLengths[segmentIndex] < dist)
        {
            segmentIndex ++;
        }
        return segmentIndex;
    }


    public Vector3 GetPositionAtDistance(float dist)
    /* Return a position on the path corresponding to a given distance traveled along the path
       PLEASE NOTE:
       This function does not return the exact distance due to the interpolation factor 't' 
       not mapping uniformly to the distance. For the runtime application the approximated 
       position at the specified distance can be obtained from the discretized path.
        Input:
            - float dist: The distance for which to find a path position
        Output:
            - Vector3: A corresponding world position along the path
    */
    {
        // First check if the distance is backwards part, or multiple loops, and correct for it     
        if (dist < 0f)
        {
            if (isClosed)
            {
                dist += PathLength * Mathf.Ceil((-1f*dist)/PathLength);
            }
            else
            {
                return points[0];
            }
        }

        // Check if the distance is more than one loop or multiple loops, and correct for it
        if (dist > PathLength)
        {
            if (isClosed)
            {
                float loops = Mathf.Floor(dist/PathLength);
                dist -= loops*PathLength;
            }
            else
            {
                return points[points.Count-1];
            }
        }
        // Determine the path segment that contains the distance
        int si = GetSegmentIndexFromDistance(dist);
        Vector3[] bpoints = GetPointsInSegment(si);
        float startDist = (si==0) ? 0f : segmentLengths[si-1];
        float segLengh = segmentLengths[si] - startDist;
        dist -= startDist;
        // Evaluate the bezier spline with a 't' that is a fraction of the remaining dist and the segment length
        float t = dist / segLengh;
        return Bezier.EvaluateCubic(bpoints[0], bpoints[1], bpoints[2], bpoints[3], t);
    }


    public Vector3[] CalculateEvenlySpacedPoints(float spacing, float sampleResolution=100f)
    /* Calculate evenly distanced points in space along the path.
       Since sampling the path with equally spaced bezier factor 't' does not result in
       equally spaced spacial points, this needs to be found iteratively.
        Input:
            - float spacing: The desired spacial spacing between points
            - float sampleResolution: The resolution with which to sample the bezier spline.
                                      Corresponds to the amount of steps with which to sample
                                      't' from 0 to 1 
        Output:
            - Vector3[]: An array of equally spaced points along the path
    */
    {
        List<Vector3> ePoints = new List<Vector3>();
        ePoints.Add(points[0]);
        Vector3 lastPoint = points[0];
        Vector3 newPoint;
        float dstSinceLastPoint = 0f;

        for (int si = 0; si < NumSegments; si++)
        // Loop over each path segment
        {
            Vector3[] p = GetPointsInSegment(si);
            float t = 0f;
            while (t <= 1f)
            // For each path segment, loop over 't' with a resolution of 'sampleResolution'
            {
                t += 1f/sampleResolution;
                newPoint = Bezier.EvaluateCubic(p[0], p[1], p[2], p[3], t);
                // Keep increasing the distance traveled until a new point is added
                dstSinceLastPoint += Vector3.Distance(lastPoint, newPoint);

                while (dstSinceLastPoint >= spacing)
                // while one or multiple equally spaced points fit inside the distance
                {
                    // Remove one spacing from the distance
                    float overshootDst = dstSinceLastPoint - spacing;
                    // Add a new point at the correct spacing interval
                    Vector3 newESP = newPoint + (lastPoint - newPoint).normalized * overshootDst;
                    ePoints.Add(newESP);
                    // Update the distance and continue the loop
                    dstSinceLastPoint = overshootDst;
                    lastPoint = newESP;
                }
                lastPoint = newPoint;
            }
        }
        return ePoints.ToArray();
    }


    public void ToggleClosed()
    /* Toggle the isClosed flag and change the path accordingly
    */
    {
        isClosed = !isClosed;
        if (isClosed)
        // If the path becomes closed two control points should be added 
        // these are two additional tangent control points between the last and first two points
        {
            points.Add(points[points.Count-1]*2 - points[points.Count-2]);
            points.Add(points[0]*2 - points[1]);
            segmentLengths.Add(0f); //Value doesn't matter as it will be set with UpdatePathLengths()
            if (autoSetControlPoints)
            // The tangent control points should be auto updated if enabled
            {
                AutoSetAnchorControlPoints(0);
                AutoSetAnchorControlPoints(points.Count-3);
            }
        }
        else
        // If the path becomes open the last two control points in the list should be removed
        {
            points.RemoveRange(points.Count-2, 2);
            AutoSetStartEndControls();
            segmentLengths.RemoveAt(segmentLengths.Count-1);
        }
        UpdatePathLegths();
    }


    void AutoSetAllAffectedControlPoints(int updatedAnchorIndex)
    /* Only update tangent control points of a specific anchor point
        Input:
            - int updatedAnchorIndex: the index of the anchor point that was changed
    */
    {
        for (int i = updatedAnchorIndex-3; i <= updatedAnchorIndex+3; i+=3)
        {
            if (i >= 0 && i < points.Count || isClosed)
            {
                AutoSetAnchorControlPoints(LoopIndex(i));
            }   
        }
        AutoSetStartEndControls();
    }

    void AutoSetAllControlPoints()
    // Auto set all tangent control points
    {
        for (int i = 0; i < points.Count; i+=3)
        {
            AutoSetAnchorControlPoints(i);
        }
        AutoSetStartEndControls();
    }

    void AutoSetAnchorControlPoints(int anchorIndex)
    /* Calculate and change new tangent control points around a specific anchor point
        Input:
            - int anchorIndex: The index of the anchor point for which its associated tangent points should be changed
    */
    {
        Vector3 anchorPos = points[anchorIndex];
        Vector3 dir = Vector3.zero;
        float[] neighbourDistance = new float[2];
        if (anchorIndex - 3 >= 0 || isClosed)
        {
            Vector3 offset = points[LoopIndex(anchorIndex-3)] - anchorPos;
            dir += offset.normalized;
            neighbourDistance[0] = offset.magnitude;
        }
        if (anchorIndex + 3 < points.Count || isClosed)
        {
            Vector3 offset = points[LoopIndex(anchorIndex+3)] - anchorPos;
            dir -= offset.normalized;
            neighbourDistance[1] = -offset.magnitude;
        }

        dir.Normalize();

        for (int i = 0; i<2; i++)
        {
            int controlIndex = anchorIndex + i * 2 - 1;
            if (controlIndex >= 0 && controlIndex < points.Count || isClosed)
            {
                points[LoopIndex(controlIndex)] = anchorPos + dir * neighbourDistance[i] * .5f;
            }
        }
    }


    void AutoSetStartEndControls()
    // Calculate and change the first and last control points of an open path
    {
        if (!isClosed)
        {
            points[1] = (points[0] + points[2]) * .5f;
            points[points.Count-2] = (points[points.Count-1] + points[points.Count-3]) * .5f;
        }
    }


    int LoopIndex(int i)
    /* Loop the point index around a round if the index is more than one or more compete circles
        Input:
            - int i: The unmodified point index
        Output:
            - int: The looped point index based 
    */
    {
        return (i + points.Count) % points.Count;
    }
}
