﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathCreator : MonoBehaviour
{

    [HideInInspector]
    public Path path;
    public float distPositionTest=0f;
    public float markerSize=1f;

    public void CreatePath()
    {
        path = new Path(transform.position);
    }

}
