﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeController : MonoBehaviour
{
    public GameObject pedalAssembly;
    public GameObject leftPedal;
    public GameObject rightPedal;
    public GameObject frontWheel;
    public GameObject backWheel;
    public GameObject steeringAssembly;
    public GameObject pathObject;
    public float wheelDiameter = 5.8f;
    public float frontWheelContact = 8.55f;
    public float frontWheelDirection = 9.81f;
    public int circleSteps = 40;
    public float maxVelocity = 3f;
    public float deceleration = 0.5f;
    public float velocityMultiplier = 1f;
    public float pedalAccelerationFactor = 7f;
    public bool flipOrientation = true;


    private PathDiscretizor path;
    private float step;
    private int pedalPosition = 0;
    // [DebugGUIGraph(min: -1, max: 1, r: 0, g: 1, b: 0, autoScale: true)]
    private float speed;
    private float speedIncrease;
    private float distance;
    private float pedalTracker = 0f;
    // [DebugGUIGraph(min: -1, max: 1, r: 1, g: 0, b: 0, autoScale: true)]
    private float pedalSpeed = 0f;
    private float scaledWR = 0f;
    private float steeringAngle;


    void Start()
    {
        step = 360f / (float)circleSteps;
        speed = 0f;
        speedIncrease = 0f;
        distance = 0f;
        scaledWR = (wheelDiameter/2) * transform.localScale[0];
        steeringAngle = steeringAssembly.transform.localEulerAngles.z;
        path = pathObject.GetComponent<PathDiscretizor>();
        // MoveAlongPath(0f);
    }


    void FixedUpdate()
    {
        PedalMotion();
        speedIncrease = (pedalSpeed/360) * velocityMultiplier * 2 * Mathf.PI * scaledWR;
        if (pedalSpeed > 0f && speedIncrease >= speed)
        {
            speed = speedIncrease;
        }
        if (speed > 0f)
        {
            speed -= speed * Time.deltaTime * deceleration;
        }
        speed = Mathf.Clamp(speed, 0f, maxVelocity);
        // DebugGUI.Graph("speed", speed);
        distance += speed;
        MoveAlongPath(distance);
        float wheelAngle = 360 * (speed / (2*Mathf.PI * scaledWR));
        frontWheel.transform.Rotate(0f, 0f, wheelAngle, Space.Self);
        backWheel.transform.Rotate(0f, 0f, wheelAngle, Space.Self);
    }


    void Update()
    {
        if (Input.GetKeyDown("up"))
        {
            IncrementPedal();
        }

        if (Input.GetKeyDown("down"))
        {
            DecrementPedal();
        }
    }


    public void IncrementPedal()
    {
        pedalPosition++;
        // print($"pedalPosition: {LoopedPos(pedalPosition)}");
    }

    public void DecrementPedal()
    {
        pedalPosition--;
        // print($"pedalPosition: {LoopedPos(pedalPosition)}");   
    }


    public void MoveAlongPath(float dist)
    {
        Vector3 pos = path.GetPositionFromDistance(dist);
        transform.position = pos;
        Vector3 lookPos = path.GetPositionFromDistance(dist+frontWheelContact*transform.localScale[0]);
        transform.LookAt(lookPos, Vector3.up);
        if (flipOrientation)
        {
            transform.RotateAround(transform.position, transform.up, 90f);
        }
        OrientFrontWheel(dist, lookPos);

    }


    public void OrientFrontWheel(float dist, Vector3 wheelContact)
    {
        Vector3 directionVec = path.GetPositionFromDistance(dist+frontWheelDirection*transform.localScale[0]) - wheelContact;
        directionVec = Vector3.ProjectOnPlane(directionVec, Vector3.up);
        Vector3 bikeDirectionVec = Vector3.ProjectOnPlane(wheelContact-transform.position, Vector3.up);
        float angle = Vector3.SignedAngle(bikeDirectionVec, directionVec, steeringAssembly.transform.up);
        steeringAssembly.transform.localEulerAngles = new Vector3(0f, angle, steeringAngle);
    }


    public void PedalMotion()
    {
        pedalSpeed = (pedalPosition*step - pedalTracker) * pedalAccelerationFactor * Time.deltaTime;
        pedalTracker += pedalSpeed;
        // DebugGUI.Graph("pedalSpeed", pedalSpeed);

        pedalAssembly.transform.Rotate(0f, 0f, pedalSpeed, Space.Self);
        leftPedal.transform.Rotate(0f, 0f, -pedalSpeed, Space.Self);
        rightPedal.transform.Rotate(0f, 0f, -pedalSpeed, Space.Self);
    }
  

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Vector3 fwp = transform.position;
        Vector3 fwdir = transform.position;
        if (flipOrientation)
        {
            fwp.x -= frontWheelContact*transform.localScale[0];
            fwdir.x -= frontWheelDirection*transform.localScale[0];
        }
        else
        {
            fwp.z += frontWheelContact*transform.localScale[0];
            fwdir.z += frontWheelDirection*transform.localScale[0];
        }

        Gizmos.DrawSphere(fwp, 0.25f*transform.localScale[0]);
        Gizmos.DrawSphere(fwdir, 0.25f*transform.localScale[0]);

    }

int LoopedPos(int pos)
{
  while (pos >= circleSteps)
  {
    pos -= circleSteps;  
  }

  while (pos < 0)
  {
    pos += circleSteps;
  }
  return pos;
}


}







// Legacy
    // void Update()
    // {
    //     speed += 2*Mathf.PI * (wheelDiameter/2) * velocityMultiplier * Time.deltaTime * rps;
    //     speed -= deceleration * speed * Time.deltaTime;
    //     speed = Mathf.Clamp(speed, 0f, maxVelocity);
    //     DebugGUI.Graph("speed", speed);
    //     distance += speed;
    //     MoveAlongPath(distance);
    //     float wheelAngle = 360 * ((speed / 2*Mathf.PI*(wheelDiameter/2))*Time.deltaTime);
    //     frontWheel.transform.Rotate(0f, 0f, wheelAngle, Space.Self);
    //     backWheel.transform.Rotate(0f, 0f, wheelAngle, Space.Self);
       

    //     if (Input.GetKeyDown("up"))
    //     {
    //         if (lastInputTime > 0f)
    //         {
    //             rps = (1f/((Time.time - lastInputTime)))/circleSteps;
    //         }

    //         DebugGUI.Graph("rps", rps);
    //         lastInputTime = Time.time;

    //         assembly.transform.Rotate(0f, 0f, step, Space.Self);
    //         leftPedal.transform.Rotate(0f, 0f, -step, Space.Self);
    //         rightPedal.transform.Rotate(0f, 0f, -step, Space.Self);
    //     }

        
    //     if (Input.GetKeyDown("down"))
    //     {
    //         assembly.transform.Rotate(0f, 0f, -step, Space.Self);
    //         leftPedal.transform.Rotate(0f, 0f, step, Space.Self);
    //         rightPedal.transform.Rotate(0f, 0f, step, Space.Self);

    //     }

    //     if (Time.time - lastInputTime > 0.5f)
    //     {
    //         rps = 0f;
    //     }

    // }
