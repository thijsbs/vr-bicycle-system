using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearSuspension : MonoBehaviour
{
    public GameObject parentController;
    public Vector3 rayOriginOffset = Vector3.zero;
    public Vector3 rayDirection = Vector3.down;
    public float maxCompression = 1f;
    public float maxExtension = 2f;
    public bool debug = false;
    public Material mat;

    private float wheelRadius;
    private float compression;
    private Vector3 startingPos;
    private GameObject hitSphere;
    private float angleFactor;
    private float lastCompression = 0f;

    private float dst = 0f;
    private float lastDst = 0f;
    private float deltaDst;


    void Start()
    {
        wheelRadius = parentController.transform.GetComponent<BikeController>().wheelDiameter;
        wheelRadius = (wheelRadius/2) * parentController.transform.localScale[0];
        startingPos = transform.localPosition;

        angleFactor = Mathf.Cos((Vector3.Angle(Vector3.up, this.transform.up)*Mathf.PI)/180f);

        if (debug && mat!=null)
        {
            hitSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            hitSphere.transform.position = transform.position;
            hitSphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            hitSphere.GetComponent<Renderer>().material = mat;
            hitSphere.layer = 2; //ignore raycasts
        }
    }


    void FixedUpdate()
    {
        RaycastHit hit;
        Ray downRay = new Ray(transform.position + rayOriginOffset, Vector3.down);        

        if (Physics.Raycast(downRay, out hit))
        {
            compression = Mathf.Clamp(hit.distance - wheelRadius, -maxCompression, maxExtension);
        }
        else
        {
            compression = 0f;
        }

        dst = Vector3.Distance(transform.localPosition - compression*Vector3.up, startingPos);
        if (dst<maxExtension && dst >-maxCompression)
        {
            transform.position -= compression/angleFactor * this.transform.up;
        }

        if (debug && mat!=null)
        {
            hitSphere.transform.position = hit.point;
            Debug.DrawLine(transform.position, hit.point, Color.red, 0.02f);
        }

    }
}
