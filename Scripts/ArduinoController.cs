// using System.IO;
using System.IO.Ports;
using System.Threading;
using UnityEngine;


public class ArduinoController
// The ArduinoController class used as a layer between the game logic and the arduino logic
{
    public SerialPort sp;               // The serialport object of the arduino controller
    public string id;                   // The unique controller ID stored in the arduino
    public int rotationSteps;           // The amount of rotation encoding steps stored in the arduino

    public bool portReady = false;      // True if the serial communication has been established
    public bool controllerReady = false;// True if the controller is fully set-up for normal operation

    public int inquiryAttempts = 3;     // The amount of attempts that are allowed to send a command to the controller before giving up

    private string posUpdate = "";      // The rotary position update sent by the arduino
    private int rDelay = 50;            // The timeout delay when requesting information from the controller (in ms)
    private int oDelay = 2000;          // The timeout delay opening the serial communication (in ms)
    private int listenTimeOut = 1;      // The timeout for listening to updates from the controller (in ms)


    public ArduinoController(string port, int baudrate, int readDelay, int openDelay)
    /* Open serial communication with the controller and set up the controller for use
        Input:
            - string port: The name of the usb serial port. Defaults to '/dev/ttyACM0' on linux
            - int baudrate: The baudrate of the communication, is set to 9600 on the controller by default
            - int readDelay: The timeout delay when requesting information from the controller
            - int openDelay: The timeout delay when opening the serial communication
    */
    {
        rDelay = readDelay;
        oDelay = openDelay;
        OpenSerial(port, baudrate);
        id = AskID();
        rotationSteps = AskSteps();
        Unblock();
    }


    void OpenSerial(string port = "/dev/ttyACM0", int baudrate = 9600)
    /* Attempt to open the serial communication through the specified port
        Input:
            - string port: The name of the usb serial port. Defaults to '/dev/ttyACM0' on linux
            - int baudrate: The baudrate of the communication, is set to 9600 on the controller by default
    */
    {
        sp = new SerialPort(port, baudrate);
        sp.ReadTimeout = listenTimeOut;
        Thread.Sleep(2000);

        try
        {
            sp.Open();
            Debug.Log($"Serial Port {sp.PortName} Opened");
            Thread.Sleep(oDelay); // Give the serial communication some time to properly setup before continuing
            portReady = true;
        }
        catch
        {
            Debug.LogError($"ERROR opening Serial Port {sp.PortName}");
        }
    }


    public void Unblock()
    /* Set the arduino controller in rotation sending mode and verify that the controller is ready
    */
    {
        if (portReady)
        {
            string response = Inquiry("u");
            // TO DO: Add a bit more flexibility and/or verbosity when unblocking does not work
            if (response == "go")
            {
                controllerReady = true;
            }
        }
    }

    public string AskID()
    /* Ask the controller for its unique ID
        Output
            - string or null: The controller ID, or null if the serial communication is not yet ready
    */
    {
        if (portReady)
        {
            string outp = Inquiry("i");
            return outp;
        }
        return null;
    }


    public int AskSteps()
    /* Ask the controller for the amount of steps it uses for encoding the rotation
        Output
            - int:  The amount of steps used by the controller to encode the rotation. 
                    Returns -1 if the serial communication is unavailable
    */
    {
        if (portReady)
        {
            string outp = Inquiry("s");
            return int.Parse(outp);
        }
        return -1;
    }


    public int GetPositionInput()
    /* Request the current rotational position of the controller
        Output:
            - int:  The current rotational position according to the controller. 
                    Returns -1 if the controller is unavailable
    */
    {
        if (controllerReady)
        {
            return int.Parse(Inquiry("p"));
        }
        return -1;
    }


    public int ListenForPositionUpdate()
    /* Listen for a rotational position update by the controller, until timed out
        Output:
            - int: The new rotational position if received in the correct format, otherwise -1
    */
    {
        if (!controllerReady)
        {
            return -1;
        }

        try
        {
            // Read any characters in the serial communications buffer
            posUpdate = sp.ReadLine();
        }
        catch (System.TimeoutException) 
        {
            return -1;
        }

        // Check if any communication has been fully received in the correct format (<00>)
        posUpdate = StripMsg(posUpdate);
        int newPos = -1;
        if (posUpdate!=null)
        {
            try
            {
                newPos = int.Parse(posUpdate);
            }
            catch
            {
                Debug.Log("Controller position update could not be parsed");
            }
        }
        return newPos;
    }


    string Inquiry(string type)
    /* Request specific information from the controller and format the answer
       All serial communication is enclosed in '<' '>' brackets to make sure all information is received properly

       List of serial commands:
        <i>:   Request arduinoID. The arduino will send back the ID (example '<0001>')
        <s>:   Request the amount of encoder steps. The arduino will send back the amount of encoder steps (ex. '<40>')
        <p>:   Request the current rotary position. The arduino will send back the current position (ex. '<38>')
        <u>:   Start the direct position communication mode. The arduino will send the current position as soon as it changes (ex. <22>)
        <b>:   Block the direct position communication mode. The arduino will not send position changes directly.
        <r00>: Set a resistance level. The code 'r' should be followed by a 2 digit number.
        
        Input:
            - string type: The type of information request command to send to the controller.

        Output:
            - string or null: The returned message, stripped of start/end chars. Or null if no such message was received
                            - 
    */
    {
        for (int i = 0; i<inquiryAttempts; i++)
        {
            try
            {
                sp.Write($"<{type}>");
                Thread.Sleep(rDelay);// Wait a sufficient amount of time for a response
            }
            catch (System.Exception)
            {
                Debug.LogError("ERROR writing to the Serial Port");
            }

            try
            {
                string outp = sp.ReadLine();

                outp = StripMsg(outp);
                if(outp==null)
                {
                    continue;
                }

                return outp;
            }
            catch (System.Exception)
            {
                Debug.LogError("ERROR reading from the Serial Port");
            }

            Debug.LogError($"ERROR: Expected answer to inquiry {type} not returned.");
        }
        return null;
    }


    string StripMsg(string msg, char startChar='<', char endChar='>')
    /* Determine if the message has been properly formatted and return the message stripped of start/end chars
    Messages are not always complete and sometimes other characters are present in the buffer before or after the message
        Input:
            - string msg: The raw input message to check and strip
            - char startChar: The starting character of the message, defaults to '<'
            - char endChar: The end character of the message, defaults to '>'
        Output:
            - string or null: The message stripped of additional characters, or null if the message was not formatted properly
    */
    {
        int start = -1;
        int end = -1;
        for (int j = 0; j<msg.Length-1; j++)
        {
            if(msg[j] == startChar)
            {
                start = j;
            }
            if(start>=0 && msg[j] == endChar)
            {
                end = j;
                break;
            }
        }

        if(start<0 || end<0)
        // The incomming message was incomplete
        {
            return null;
        }
        return msg.Substring(start+1, end-start-1);
    }
}
